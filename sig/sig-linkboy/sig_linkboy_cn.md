# SIG-linkboy
简体中文 | [English](./sig_linkboy.md)

说明：本SIG的内容遵循OpenHarmony的PMC管理章程 [README](/zh/pmc.md)中描述的约定。

## SIG组工作目标和范围

### 工作目标
* 技术层面：联合志愿者协同开展后续linkboy对OpenHarmony系统的适配和移植
* 应用层面：基于linkboy社区的已有用户群，推广OpenHarmony系统

### 工作范围
* 编译器后端指令集适配
* 完善linkboy组件适配
* OpenHarmony组件图形化封装
* OpenHarmony组件仿真支持
* 各厂家OpenHarmony开发板图形化封装


## 代码仓
- 代码仓地址：
  - linkboy：https://gitee.com/openharmony-sig/linkboy

## SIG组成员

### Leader
- [linkboy_crux](https://gitee.com/linkboy_crux)

### Committers列表
- [lcm](https://gitee.com/lcm)
- [chaoyangc](https://gitee.com/chaoyangc)
- [ownery](https://gitee.com/ownery)

### 会议
 - 会议时间：双周例会，周五 14:00
 - 会议申报：[SIG-linkboy会议申报](https://shimo.im/sheets/sX5pBO7PwFkEsR1D)
 - 会议链接：腾讯会议或其他会议
 - 会议通知：请[订阅](https://lists.openatom.io/postorius/lists/sig_linkboy.openharmony.io)邮件列表获取会议链接
 - 会议纪要：查看往期会议纪要，请点此[链接](https://gitee.com/openharmony-sig/sig-content/tree/master/linkboy/meetings)

### 联系方式(可选)

- 邮件列表：[sig_linkboy@openharmony.io](https://lists.openatom.io/postorius/lists/sig_linkboy.openharmony.io/)
- Slack群组：xxx
- 微信群：xxx
